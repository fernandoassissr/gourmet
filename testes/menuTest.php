<?php
/**
 * Testar a classe menu
 *
 * @author  Fernando de Assis
 */

$rootPath = dirname(__DIR__);
require_once $rootPath.'/src/menu.php';

class MenuTest extends \PHPUnit_Framework_TestCase
{
    public function test__construct()
    {        
        $menuObject = new Menu('massa','lasanha');        
        $this->assertEquals('massa', $menuObject->getTipo());
        $this->assertEquals('lasanha', $menuObject->getPrato());
    }
}
