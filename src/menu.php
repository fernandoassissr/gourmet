<?php
/**
 * Classe Menu - onde temos os nossos pratos
 *
 * @author  Fernando de Assis
 */

class Menu
{
	protected $tipo, $prato;
	
	function __construct($tipo, $prato)
	{
		$this->setTipo($tipo);
		$this->setPrato($prato);
	}

	public function setTipo($tipo)
	{
		$this->tipo = $tipo;
	}

	public function setPrato($prato)
	{
		$this->prato = $prato;
	}

	public function getTipo()
	{
		return $this->tipo;
	}

	public function getPrato()
	{
		return $this->prato;
	}


}